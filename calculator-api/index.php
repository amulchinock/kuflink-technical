<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
header ('Content-type: text/html; charset=utf-8');

// Addition - Alien U+1F47D %F0%9F%91%BD
// Subtraction - Skull  U+1F480 	%F0%9F%92%80
// Multiplicaion - Ghost  U+1F47B  %F0%9F%91%BB
// Division - Scream  U+1F631 	%F0%9F%98%B1

// Calculations represented as URL string in get param
// each character is seperated by a pipe (|)
// parenthesis should not be encoded - for readability...
// Example (5 * 5) - 2:
//    (|5|%F0%9F%91%BB|5|)|%F0%9F%92%80|2

$alg = explode('|', $_GET['alg']);
$algString = '';

foreach ($alg as $step) {
  switch ($step) {
    // Addition
    case "\u{1F47D}":
    $algString .= '+';
    break;

    // Subtraction
    case "\u{1F480}":
    $algString .= '-';
    break;

    // Multiplication
    case "\u{1F47B}":
    $algString .= '*';
    break;

    // Division
    case "\u{1F631}":
    $algString .= '/';
    break;

    default:
    // Only allows characters 0 - 9 and parenthesis
    if (preg_match('([0-9]|\(|\))', $step)) {
      $algString .= $step;
    }
    break;
  }
}

// Using eval() is potentially dangerous, as it opens us up to injection attacks.
// If this were a production piece of code, this would probably not be used...
$response = [];
try {
  http_response_code(200);
  $answer = 0;
  eval('$answer = '.$algString.';');

  $response = [
    'answer' => substr_compare( $algString, '/0', -2 ) === 0 ? 'You may not divide by zero. You know not of the dangers, mortal.' : round($answer, 10),
    'request' => $_GET['alg'],
    'equation' => $algString
  ];
} catch (Throwable $e) {
  http_response_code(500);
  // echo $e;
  $response = [
    'error' => $e,
    'request' => $_GET['alg'],
    'equation' => $algString
  ];
}

print_r(json_encode($response));
