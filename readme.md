# Hello.
To get started, follow both sections below:

**Important: Please ensure that you do not currently have an instance of localhost running on ports 8080 or 8081 on your local machine.**

## Running the API
To run the API, do the following:

  - Open a terminal window, and navigate to ```./calculator-api``` within this project

  - Run the following command ```php -S localhost:8081```, to spin up PHP's dev server for this project.

The API will be available at ```http://localhost:8081```

## Running the client
For speed, I built the client in Vue. Do the following steps to get started working on the client:

  - Open a new terminal window or terminal tab and navigate to ```./calculator-ui``` within this project

  - Run ```npm install``` to install client's dependancies (you should only need to do this once, not every time you spin this up)

  - Run the following command ```npm run serve```, to spin up the client's dev server.

The client will be available at ```http://localhost:8080```

## Notes
Here's an explanation of some of my thought processes whilst bulding the API and client:

### API
  - My initial thought was to allow a user to send very simple equations to the API - in line with the examples given in the test specifiaction. However - I was intruiged by the idea of being able to send across slightly more complex equations (using parenthesis), and so had to come up with a way to achieve this.

  - Due to the fact that URL encoded unicode characters (among other types of characters) actually consist of more than 1 character - I couldn't rely on simply looking for operations characters and trying to pick the numbers out from between them. As such, I decided to allow the API to accept a pipe seperated string of numbers and operands, so we could easily determine whole character sets. (e.g ```(|5|%F0%9F%91%BB|5|)|%F0%9F%92%80|2|```)

  - From there, it was simply a task of splitting the string by it's seperators, and looking out for the unicode characters we would need to replace with their valid mathmatical counterparts.

  - Once this was completed - we had a human-readable equation to work with: ```(5 * 5) - 2```

  - This, of course causes a further issue. PHP won't see this as a valid mathmatical thing (for lack of better wording). All it will see is a string. At this point, I had two schools of thought:

    1.  Write a utility method that would parse this string, whilst respecting the orders of operations (BODMAS).

    2. Look for a quick and dirty way of getting PHP to do this for me.

  I settled on the second option, based on the fact that this is supposed to be a proof of concept. PHP's ```eval()``` method proved to be very useful here, however - in a real-world scenario, this would potentially be a bad idea; as this would open our application up to a major risk of injection-type attacks.

  - I noticed, when testing the API, that attempting to divide by zero would present us with a blank string as an answer (I was testing from the client at this point, so one might have seen more valuable information from within Postman or a similar service). In light of this, I added a little 'easter egg' as a way to handle this. It was intended in good humor - and obviously wouldn't have been done had this been a 'real' project.

### Client
  - Due to my familiarity with Vue, and knowing how nice it can be for working with APIs - I felt it would be appropriate (if not overkill) to use it to build the client. I appreciate that this aspect isn't being assessed.

  - For speed, I found a Codepen (https://codepen.io/jsalazart/pen/jJWMVw) with a working calculator component, and modified it to fit my needs.

  - Rather than write an API call from stratch, I decided to use Axios to handle the request and response from the client to the API.

  - It is possible to send a nonsensical equation to the API from the client (e.g ```5(1``` ), as I haven't implemented any sanity checks in the calculator. The API however, will catch this error.
